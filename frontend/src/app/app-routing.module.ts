import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MyTimetableComponent } from './my-timetable/my-timetable.component';

const routes: Routes = [
  {path: '', component: MyTimetableComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
